/* 
Copyright (c) 2020, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
 * Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
 * Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Babu PS
Email id: babu.ps@hotmail.com
Details:

--------------------------------------------------------------------------------------------------*/
package bmu_scop_wrapper;

  import GetPut :: *; 
  import FIFO :: *;
  import FIFOF :: *;
  import SpecialFIFOs :: *;
  import DReg :: *;
  import TxRx :: *;
  import Vector :: *;

  // project imports
  import AXI4_Types:: *;
  import AXI4_Fabric:: *;
  import AXI4_Lite_Types:: *;
  import AXI4_Lite_Fabric:: *;
  import common_types::*;
  import csr::*;
  `include "common_params.bsv"
  `include "Logger.bsv"
  import bitmanip       :: *;

  interface Ifc_bmu_scop_accelarator;  
    interface Put#(Stage3Scop) rec_req;
    interface Get#(Scop_resp) core_resp;
    method Bool mv_scop_busy;    
  endinterface

  module mk_bmu_accelarator(Ifc_bmu_scop_accelarator);
    Ifc_bitmanip bitmanip <- mkbitmanip ;     
    FIFOF#(Stage3Scop ) ff_scop_req <- mkSizedFIFOF(2);
    FIFOF#(Scop_resp) ff_scop_resp <- mkSizedFIFOF(2);

    rule rl_bmu_recv_inputs;
      ff_scop_req.deq;
      let request  = ff_scop_req.first;
//      $display("BMU Ifc :Received Input :%h",request.inst);
      `logLevel( bmu_scop_accelerator, 0, $format("BMU : Received Request : ", fshow(request)))
      let resp  <- bitmanip.mav_putvalue(request.inst,request.op[0],request.op[1],request.op[2]);
      ff_scop_resp.enq(Scop_resp{cause: (tpl_2(resp))? 0:`Illegal_inst,trap:!tpl_2(resp),badaddr:0,data:tpl_1(resp),user:? });
    endrule

  interface  rec_req = interface Put
  method Action put (Stage3Scop request);
    ff_scop_req.enq(request);
  endmethod
  endinterface;

  interface core_resp= interface Get
  method ActionValue#(Scop_resp) get ;
    ff_scop_resp.deq;
    return ff_scop_resp.first;
  endmethod
  endinterface;

  method mv_scop_busy = bitmanip.mv_scopbusy;

  endmodule

  endpackage:bmu_scop_wrapper







