/* Copyright (c) 2019, IIT Madras All rights reserved. Redistribution and use
in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:
  * Redistributions of source code must retain the above copyright notice,
    this list of conditions   and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and / or other materials provided   with the distribution.
  * Neither the name of IIT Madras  nor the names of its contributors may
    be used to endorse or promote products derived from this software without
    specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.
-------------------------------------------------------------------------------
 Author : Babu P S, Snehashri Sivaraman
 Details: Implementation of Bit Manipulation Instructions (BMI) as IP.
 Instructions implemented here are the subset of instructions proposed by
 the riscv/bit-manip group. Implementation follows bitmanip-v0.93.
-------------------------------------------------------------------------------
*/

package bitmanip;

import Vector :: * ;
import FIFOF :: *;


`include "bmi.defines"
`include "Logger.bsv"
`include "compute.bsv"

`ifdef RV64
   typedef 64 XLEN;
   typedef 32 XLEN_BY_2;
`else
   typedef 32 XLEN;
`endif
typedef Bit#(7) Imm;
typedef Bit#(5) Imm1;
typedef Reg#(Bool) Flag;
typedef struct {
  Bit#(32) instr;
  Bit#(XLEN) src1;
  Bit#(XLEN) src2;
  Bit#(XLEN) src3;
} Command deriving (Bits, Eq, FShow);



interface Ifc_bitmanip;
  method ActionValue#(Tuple2#(Bit#(XLEN), Bool)) mav_putvalue(Bit#(32) instr, Bit#(XLEN) src1, Bit#(XLEN) src2, Bit#(XLEN) src3);
  method Bool mv_scopbusy;
endinterface
(*synthesize*)
/*doc:module: implements the BitManip Instructions */
module mkbitmanip(Ifc_bitmanip);

    method ActionValue#(Tuple2#(Bit#(XLEN), Bool)) mav_putvalue(Bit#(32) instr, Bit#(XLEN) src1, Bit#(XLEN) src2, Bit#(XLEN) src3);
      Tuple2#(Bit#(XLEN), Bool) result = tuple2( ?, False);
      let opcode = pack(instr)[6:0];
      if(opcode == 7'b0110011 || opcode == 7'b0010011 `ifdef RV64 || opcode == 7'b0111011 || opcode == 7'b0011011 `endif )
        result = fn_compute(opcode,pack(instr)[31:25],pack(instr)[14:12],pack(src1),pack(src2),pack(src3),pack(instr)[26:20]);
      return result;
    endmethod

    method mv_scopbusy = False;

endmodule: mkbitmanip

endpackage: bitmanip

