`include "bmi.defines"
`include "Zbb.bsv"
`include "Zbs.bsv"
`include "Zba.bsv"
`include "Zbe.bsv"
`include "Zbr.bsv"
`include "Zbt.bsv"
`include "Zbf.bsv"
`include "Zbp.bsv"
`include "Zbc.bsv"
`include "Zbm.bsv"
typedef enum {NULL,SHIFT,ARITH,FUNNEL,SINGL_BIT_OP,CRC32, SHNADD} Instr_set deriving(Eq,Bits);

function Tuple2#(Bit#(XLEN), Bool) fn_compute(Bit#(7) opcode, Bit#(7) funct7, Bit#(3) funct3, Bit#(XLEN) rs1,Bit#(XLEN) rs2, Bit#(XLEN) rs3,Bit#(7) imm);
   Bit#(22) regdest;
   Tuple2#(Bit#(XLEN), Bool) result  = tuple2(?, True);
   Instr_set iset=NULL;
   regdest = {funct7,imm[4:0],funct3,opcode};
   Bit#(XLEN) field1= tpl_1 ( result ) ;
   Bool field2 = tpl_2 (result);
                   case(regdest) matches
`ifdef ZBB            `BMI_AND_N_INSTR  :   field1  = fn_andn(rs1,rs2);
                      `BMI_OR_N_INSTR   :   field1  = fn_orn(rs1,rs2);
                      `BMI_XOR_N_INSTR  :   field1  = fn_xorn(rs1,rs2);
                      `BMI_SLO_INSTR    :   iset=SHIFT;
                      `BMI_SLOI_INSTR   :   iset=SHIFT;
                      `BMI_SRO_INSTR    :   iset=SHIFT;
                      `BMI_SROI_INSTR   :   iset=SHIFT;
                      `BMI_ROL_INSTR    :   iset=SHIFT;
                      `BMI_ROR_INSTR    :   iset=SHIFT;
                      `BMI_RORI_INSTR   :   iset=SHIFT;
                      `BMI_PACKL_INSTR  :   field1  = fn_pack_lower(rs1,rs2,opcode[3]);
                      `BMI_PACKU_INSTR  :   field1  = fn_pack_upper(rs1,rs2,opcode[3]);
                      `BMI_PACKH_INSTR  :   field1  = fn_packh(rs1,rs2);
                      `BMI_MIN_INSTR    :   field1  = fn_min(rs1,rs2,funct3[1:0]);
                      `BMI_CLZ_INSTR    :   field1  = fn_clz(rs1,opcode[3]);
                      `BMI_CTZ_INSTR    :   field1  = fn_ctz(rs1,opcode[3]);
                      `BMI_PCNT_INSTR   :   field1  = fn_pcnt(rs1,opcode[3]);
                      `BMI_SEXT_B_INSTR :   field1  = fn_sext_b(rs1);
                      `BMI_SEXT_H_INSTR :   field1  = fn_sext_h(rs1);
`ifdef RV64
                      `BMI_ADDWU_INSTR   :   field1  = fn_addwu(rs1, rs2,funct7[5],funct7[0],funct3[2]);
                      `BMI_ADDIWU_INSTR  :  iset=ARITH;
                      `BMI_SLLIU_W_INSTR :  iset=SHIFT;`endif
                      `endif

`ifdef ZBS            `BMI_SBCLR_INSTR  :   iset=SINGL_BIT_OP;
                      `BMI_SBCLRI_INSTR :   iset=SINGL_BIT_OP;
                      `BMI_SBEXT_INSTR  :   iset=SINGL_BIT_OP;
                      `BMI_SBEXTI_INSTR :   iset=SINGL_BIT_OP;
                      `BMI_SBINV_INSTR  :   iset=SINGL_BIT_OP;
                      `BMI_SBINVI_INSTR :   iset=SINGL_BIT_OP;
                      `BMI_SBSET_INSTR  :   iset=SINGL_BIT_OP;
                      `BMI_SBSETI_INSTR :   iset=SINGL_BIT_OP;  `endif

`ifdef ZBA            `BMI_SH1ADD_INSTR :   iset= SHNADD;
                      `BMI_SH2ADD_INSTR :   iset= SHNADD;
                      `BMI_SH3ADD_INSTR :   iset= SHNADD; `endif

`ifdef ZBE            `BMI_BEXT_INSTR    :   field1  = fn_bext(rs1,rs2);
                      `BMI_BDEP_INSTR    :   field1  = fn_bdep(rs1,rs2);
`ifdef RV64           `BMI_BEXTW_INSTR   :   field1  = fn_bextw(rs1,rs2);
                      `BMI_BDEPW_INSTR   :   field1  = fn_bdepw(rs1,rs2);`endif  `endif
`ifdef ZBP            `BMI_GREV_INSTR    :   field1  = fn_grev(rs1, rs2);
                      `BMI_GORC_INSTR    :   field1  = fn_gorc(rs1, rs2);
                      `BMI_SHFL_INSTR    :   field1  = fn_shfl(rs1 , rs2);
                      `BMI_UNSHFL_INSTR  :   field1  = fn_unshfl(rs1 , rs2);
                      `BMI_GREVI_INSTR   :   field1  = fn_grevi(rs1, imm[6:0]);
                      `BMI_GORCI_INSTR   :   field1  = fn_gorci(rs1, imm[6:0]);
                      `BMI_SHFLI_INSTR   :   field1  = fn_shfli(rs1 , imm[5:0]);
                      `BMI_UNSHFLI_INSTR :   field1  = fn_unshfli(rs1 , imm[5:0]);
`ifdef RV64           `BMI_GREVW_INSTR   :   field1  = fn_grevw(pack(rs1)[31:0], pack(rs2)[31:0]);
                      `BMI_GORCW_INSTR   :   field1  = fn_gorcw(pack(rs1)[31:0], pack(rs2)[31:0]);
                      `BMI_SHFLW_INSTR   :   field1  = fn_shflw(pack(rs1)[31:0] , pack(rs2)[31:0]);
                      `BMI_UNSHFLW_INSTR :   field1  = fn_unshflw(pack(rs1)[31:0] , pack(rs2)[31:0]);
                      `BMI_GREVIW_INSTR  :   field1  = fn_greviw(pack(rs1)[31:0], imm[4:0]);
                      `BMI_GORCIW_INSTR  :   field1  = fn_gorciw(pack(rs1)[31:0], imm[4:0]);`endif `endif
`ifdef ZBC            `BMI_CLMUL_INSTR   :   field1  = fn_clmul(rs1, rs2);
                      `BMI_CLMULR_INSTR  :   field1  = fn_clmulr(rs1, rs2);
                      `BMI_CLMULH_INSTR  :   field1  = fn_clmulh(rs1, rs2);
`ifdef RV64           `BMI_CLMULW_INSTR  :   field1  = fn_clmulw(rs1[31:0], rs2[31:0]);
                      `BMI_CLMULRW_INSTR :   field1  = fn_clmulrw(rs1[31:0], rs2[31:0]);
                      `BMI_CLMULHW_INSTR :   field1  = fn_clmulhw(rs1[31:0], rs2[31:0]); `endif `endif

`ifdef ZBF            `BMI_BFP_INSTR     :   field1  = fn_bfp(rs1, rs2);
`ifdef RV64           `BMI_BFPW_INSTR    :   field1  = fn_bfpw(rs1, rs2); `endif `endif
`ifdef RV64`ifdef ZBM `BMI_BMATXOR_INSTR :   field1  = fn_bmatxor(rs1,rs2);
                      `BMI_BMATOR_INSTR  :   field1  = fn_bmator(rs1,rs2);
                      `BMI_BMATFLIP_INSTR:   field1  = fn_bmatflip(rs1); `endif        `endif
`ifdef ZBT            `BMI_CMIX_INSTR    :   field1  = fn_cmix(rs1, rs2, rs3);
                      `BMI_CMOV_INSTR    :   field1  = fn_cmov(rs1, rs2, rs3);
                      `BMI_FSL_INSTR     :   iset=FUNNEL;
                      `BMI_FSR_INSTR     :   iset=FUNNEL;
                      `BMI_FSRI_INSTR    :   iset=FUNNEL;
`ifdef RV64           `BMI_FSLW_INSTR    :   iset=FUNNEL;
                      `BMI_FSRW_INSTR    :   iset=FUNNEL;
                      `BMI_FSRIW_INSTR   :   iset=FUNNEL; `endif `endif
`ifdef ZBR            `BMI_CRC_B_INSTR   :   iset=CRC32;
                      `BMI_CRC_H_INSTR   :   iset=CRC32;
                      `BMI_CRC_W_INSTR   :   iset=CRC32;
`ifdef RV64           `BMI_CRC_D_INSTR   :   iset=CRC32;  `endif
                      `BMI_CRCC_B_INSTR  :   iset=CRC32;
                      `BMI_CRCC_H_INSTR  :   iset=CRC32;
                      `BMI_CRCC_W_INSTR  :   iset=CRC32;
`ifdef RV64           `BMI_CRCC_D_INSTR  :   iset=CRC32; `endif `endif

                       default           : begin
                                            field1 = ?;
                                            field2 = False;
                                           end //fn_defaultcase();
                 endcase


   if(iset==SHIFT `ifdef ZBT || iset==FUNNEL `endif ) begin
      Integer immEnd = log2(valueof(XLEN));
      field1 = fn_shift_and_rotate(rs1, (unpack(opcode[5]) ? truncate(rs2) : imm[immEnd:0]), rs3, opcode[3], opcode[5], funct3[2], funct7);
//      iset=NULL;
   end
   else if(iset==SINGL_BIT_OP) begin
      Integer immEnd = log2(valueof(XLEN))-1;
      field1 = fn_set_bit_ops(rs1, (unpack(opcode[5]) ? truncate(rs2) : imm[immEnd:0]),opcode[3],{funct7[5], funct7[4], funct3[2]});
//      iset=NULL;
   end
   else if(iset==SHNADD) begin
      field1 = fn_shnadd(`ifdef RV64 unpack(opcode[3]) ? zeroExtend(rs1[31:0]) : `endif rs1, `ifdef RV64 unpack(opcode[3]) ? zeroExtend(rs2[31:0]) : `endif rs2,{funct3[2],funct3[1]});
//      iset=NULL;
   end
`ifdef ZBR
   else if(iset==CRC32) begin
   		case({imm[1],imm[0]}) matches
        2'b00: field1 = fn_crc(rs1, imm[3],8);
        2'b01: field1 = fn_crc(rs1, imm[3],16);
        2'b10: field1 = fn_crc(rs1, imm[3],32);
`ifdef RV64
        2'b11: field1 = fn_crc(rs1, imm[3],64);
`endif
    endcase
//     iset=NULL;
   end
`endif
`ifdef ZBB
  `ifdef RV64
   else if(iset==ARITH) begin
      field1 = fn_addwu(rs1,signExtend({ funct7,imm[4:0]}),funct7[5],funct7[0],funct3[2]);
      iset=NULL;
   end
  `endif
`endif
  return tuple2(field1, field2);
endfunction
