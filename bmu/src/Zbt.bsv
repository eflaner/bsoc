  /*doc:func: The cmix rd, rs2, rs1, rs3 instruction selects bits from rs1 and rs3 based on the bits in the
control word rs2.*/
  function Bit#(XLEN) fn_cmix(Bit#(XLEN) src1,Bit#(XLEN) src2, Bit#(XLEN) src3) provisos(Bitwise#(Bit#(XLEN))) ;
    return ((src1 & src2) | (src3 & (~src2)) );
  endfunction
  /*doc:func:The cmov rd, rs2, rs1, rs3 instruction selects rs1 if the control word rs2 is non-zero, and rs3
if the control word is zero.*/
  function Bit#(XLEN) fn_cmov(Bit#(XLEN) src1,Bit#(XLEN) src2, Bit#(XLEN) src3) provisos(Bitwise#(Bit#(XLEN))) ;
    return ( (src2>0) ? src1 : src3 );
  endfunction
