`ifdef RV64
 function Bit#(XLEN) fn_pcnt2(Bit#(XLEN) src1) provisos(Arith#(Bit#(XLEN))) ;
    return zeroExtend(pack(countOnes(src1)));
  endfunction
  function Bit#(XLEN) fn_shfl64c(Bit#(XLEN) src1, Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ;
        let x= src1;
        let shamt = src2 & 31;
        if ((shamt & 16) > 0) x =shuffle_stage(x, 'h0000ffff00000000, 'h00000000ffff0000, 16);
        if ((shamt & 8) > 0) x =shuffle_stage(x, 'h00ff000000ff0000, 'h0000ff000000ff00, 8);
        if ((shamt & 4) > 0) x =shuffle_stage(x, 'h0f000f000f000f00, 'h00f000f000f000f0, 4);
        if ((shamt & 2) > 0) x =shuffle_stage(x, 'h3030303030303030, 'h0c0c0c0c0c0c0c0c, 2);
        if ((shamt & 1) > 0) x =shuffle_stage(x, 'h4444444444444444, 'h2222222222222222, 1);
        return x;
    endfunction
/*doc:func: bmatflip is a unary operator that transposes the source matrix. It is equivalent to zip; zip; zip
on RV64.*/
function Bit#(XLEN) fn_bmatflip(Bit#(XLEN) rs1) provisos(Bitwise#(Bit#(XLEN))) ;
    Bit#(XLEN) x = rs1;
    x = fn_shfl64c(x, 31);
    x = fn_shfl64c(x, 31);
    x = fn_shfl64c(x, 31);
    return x;
endfunction
/*doc:func: bmatxor performs a matrix-matrix multiply with boolean AND as multiply operator and boolean
XOR as addition operator.*/
function Bit#(XLEN) fn_bmatxor(Bit#(XLEN) rs1, Bit#(XLEN) rs2) provisos(Bitwise#(Bit#(XLEN))) ;

// transpose of rs2
    Bit#(64) rs2t = fn_bmatflip(rs2);
    Vector#(8,Bit#(8)) u; // rows of rs1
    Vector#(8,Bit#(8)) v; // cols of rs2
    for (int i = 0; i < 8; i=i+1) begin
    u[i] = truncate(rs1 >> (i*8));
    v[i] = truncate(rs2t >> (i*8));
    end
    Bit#(64) x = 0;
    for (int i = 0; i < 64; i=i+1) begin
    if ((fn_pcnt2(zeroExtend(u[i / 8] & v[i % 8])) & 1) == 1)
    x = x | (1 << i);
    end

    return x;
endfunction
/*doc:func:bmator performs a matrix-matrix multiply with boolean AND as multiply operator and boolean
OR as addition operator.*/
function Bit#(XLEN) fn_bmator(Bit#(XLEN) rs1, Bit#(XLEN) rs2) provisos(Bitwise#(Bit#(XLEN))) ;

    // transpose of rs2
    Bit#(64) rs2t = fn_bmatflip(rs2);
    Vector#(8,Bit#(8)) u; // rows of rs1
    Vector#(8,Bit#(8)) v; // cols of rs2
    for (int i = 0; i < 8; i=i+1) begin
    u[i] = truncate(rs1 >> (i*8));
    v[i] = truncate(rs2t >> (i*8));
    end
    Bit#(64) x =0;
    for (int i =0; i < 64; i=i+1) begin
    if ((u[i/ 8] & v[i % 8]) != 0)
    x = x | (1 << i);
    end
    return x;
endfunction
`endif
