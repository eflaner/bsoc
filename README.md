[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# Design of bSoC

This repository builds a SoC that has Bit Manipulation Unit (BMU) containing instructions from [riscv-bitmanip](https://github.com/riscv/riscv-bitmanip/blob/master/bitmanip-draft.pdf) [cb7a84e](https://github.com/riscv/riscv-bitmanip/commit/cb7a84eb8b32ca5a98fad8ca7aa2a84a7e520a45) as coprocessor to the low power embedded class RISC-V core  [E-Class](https://gitlab.com/shaktiproject/cores/e-class/tree/83-bmu-as-shakti-co-processor-using-scop-interface) from Shakti Processors family for a bit manipulation enabled SoC - **bSoC**

* [Directory Structure](#dir-struct)
* [Prerequisites](#prerequisites)
* [Commands](#setup-commands)

## Directory Structure <a name="dir-struct"></a>

```text
design
├── e-class                     #Core
├── bmu                     	#BMU
├ 
├── tests			            #tests for SoC
├── support						#support files          
	 ├── bootcode 
	 ├── SoC.bsv				#Synthesis SoC
	 ├── soc_synthesis_config.inc
├── Soc.bsv                     #Simulation SoC
├── soc_simulation_config.inc   #Config Options for bSoC

```

### Prerequisites<a name="prerequisites"></a>

To build bSoC, specific tools are required, since most of the tools are open-source that keeps upgrading, the commit/versions details are specified. The tools were built in Ubuntu 18.04 LTS. 

- Bluespec Compiler (build e5af869)
- Verilator 4.002 2018-09-16
- Xilinx Vivado 2018.3+

Please follow the guide from shakti-soc for the necessary tools to build the bSoC - [Click here](https://gitlab.com/shaktiproject/cores/shakti-soc/-/blob/master/README.rst). 

The toolchain build should be 32-bit with bit manipulation extension enabled and can be followed up from [here](https://gitlab.com/eflaner/riscv/-/blob/master/Building%20modified%20RISC-V%20Tool-chain.md).

### Setup commands<a name="setup-commands"></a>

Update the dependencies of the repository by calling the below command.

```sh
$ ./manager.sh update_deps
make patch
```
After updating repository, there are two options to pursue. 

![Building bSoC](./support/readmeSupport/bSoC build.png)

### Simulate

To simulate the design on verilator

```sh
make simulate
```

On completion of the above command, **bin** directory contains the executable which can be used along with *code.mem* to simulate the soft-core with program as shown below

![bSoC simulation](./support/readmeSupport/bSoC simulation.png)

The program can be compiled using the modified risc-v toolchain. A starting guide to compile a baremetal code is available [**here**](https://gitlab.com/eflaner/riscv/-/blob/master/demo/Compiling%20using%20RISC-V%20Tool-chain.md). Adapt it according to the [support files](./support/embench_support/embench/config/bsoc/chips). 

### Synthesis

To synthesise as softcore on FPGA use the command

```sh
make synthesis SYN=FPGA
```

*Note: The synthesis works good for ARTIX-7 100T CSG324 for any other fpga board appropriate changes in Tcl scripts and other manoeuvring (makefile, fpga_top.v, etc. ) is recommended.*

![bSoC Synthesis](./support/readmeSupport/bSoC synthesis.png)

After synthesis, reset the board and use command in terminal 

```
sudo miniterm /dev/ttyUSB1 19200
```

On pressing the reset button, the bootscreen should appear, with a hello-world on screen.

<img src="./support/readmeSupport/bootscreen.png" alt="Bootsplash" style="zoom:60%;" />

A simple LED Blinking Example on bSoC is shown below. 

<img src="./support/readmeSupport/blinky_example.gif" alt="Bootsplash" style="zoom:80%;" />

----
### validation of bSoC for compliance (Optional)

To checks whether the created SoC is complaint with the RISC-V specification.

```sh
make compliance 
```

if an error ***module not found*** is reported try `make setup-riscof-env` and re-initiate compliance test.

### Results

The developed bSoC is [tested](./support/readmeSupport/results.md) against standard SoC using Embench<sup>TM</sup>.

## Author

* **Babu P S** - [eflaner](https://gitlab.com/eflaner)

## License

This project is licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).

## Acknowledgements

*This project is built on Shakti family of processors.*

E-class is derived from [cc6e99da1315ec2f0fdd82153121c47aab8f5828](https://gitlab.com/shaktiproject/cores/e-class/-/tree/83-bmu-as-shakti-co-processor-using-scop-interface) 

BMU is derived from [97303421bf838ceb75046ff8e6b8ec8c4509250b](https://gitlab.com/shaktiproject/cores/bbox/-/tree/62-optimizations-in-bbox32)

Embench from [bc7a9d6bf399dd5a90a613bb313c078784f4847d](https://github.com/embench/embench-iot/tree/master)