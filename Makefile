## 			Makefile for bSoC

ifeq (, $(wildcard ./old_vars))
	old_define_macros = ""
else
	include ./old_vars
endif

ifeq ($(SYN), FPGA)
	CONFIG?=support/soc_synthesis_config.inc
else
	CONFIG?=soc_simulation_config.inc
endif

include $(CONFIG)

ifeq ($(define_macros),)
	define_macros+= -D Addr_space=25
else
ifneq (,$(findstring Addr_space,$(define_macros)))
else
	override define_macros+= -D Addr_space=25
endif
endif

# for riscof compliance operations
PYTHON_VERSION_MIN=3.6

BSC_DIR := $(shell which bsc)
BSC_VDIR:=$(subst /bsc,/,${BSC_DIR})../lib/Verilog/
BLUESPECDIR ?= $(shell cd $(subst /bsc,/,${BSC_DIR}); cd ../lib; pwd ;)

DIR_HOME=$(PWD)
export DIR_HOME

TOP_MODULE?=mkTbSoc
TOP_FILE?=TbSoc.bsv
TOP_DIR:=./
WORKING_DIR := $(shell pwd)
Vivado_loc=$(shell which vivado || which vivado_lab)

# ------------------ based on the config generate define macros for bsv compilation --------------#
ifneq (,$(findstring RV64,$(ISA)))
  override define_macros += -D RV64=True
  XLEN=64
endif
ifneq (,$(findstring RV32,$(ISA)))
  override define_macros += -D RV32=True
  XLEN=32
endif
ifneq (,$(findstring M,$(ISA)))
  ifeq ($(MUL), fpga)
    override define_macros += -D muldiv_fpga=True -D muldiv=True
  else
    override define_macros += -D $(MUL)=True -D muldiv=True
  endif
endif
ifneq (,$(findstring A,$(ISA)))
  override define_macros += -D atomic=True
endif
ifneq (,$(findstring C,$(ISA)))
  override define_macros += -D compressed=True
endif
ifeq ($(SYNTH),SIM)
  override define_macros += -D simulate=True
else
  TOP_MODULE:=mkSoc
  TOP_FILE:=support/Soc.bsv
endif
ifeq ($(COREFABRIC), AXI4Lite)
  override define_macros += -D CORE_AXI4Lite=True
endif
ifeq ($(USERTRAPS), enable)
  override define_macros += -D usertraps=True
endif
ifeq ($(SCOP), enable)
  override define_macros += -D scop=True
  ifneq (,$(findstring B,$(ISA)))
  	override define_macros += -D bmu=True
  	ifeq ($(BM_EXTNS), ALL)
  		override define_macros += -D ZBB=True -D ZBA=True -D ZBS=True -D ZBC=True -D ZBM=True -D ZBT=True -D ZBR=True -D ZBE=True -D ZBP=True -D ZBF=True
  	else
  		override define_macros += -D $(BM_EXTNS)=True
  	endif
  endif
endif
ifeq ($(USER), enable)
  override define_macros += -D user=True
endif
ifeq ($(RTLDUMP), enable)
  override define_macros += -D rtldump=True
endif
ifeq ($(ASSERTIONS), enable)
  override define_macros += -D ASSERT=True
endif
ifeq ($(PMP), enable)
	override define_macros += -D pmp=True
endif
ifeq ($(ARITHTRAP), enable)
	override define_macros += -D arith_trap=True
endif
ifeq ($(DEBUG), enable)
	override define_macros += -D debug=True
endif
ifeq ($(SIGN_EN), enable)
	override define_macros += -D sign_en=True
endif
ifeq ($(OPENOCD), enable)
	override define_macros += -D openocd=True
endif
ifeq ($(BSCAN2E), enable)
	override define_macros += -D bscan2e=True
	JTAG_TYPE:=JTAG_BSCAN2E
else
	JTAG_TYPE:=JTAG_EXTERNAL
endif
ifneq ($(TRIGGERS), 0)
	override define_macros += -D triggers=True -D trigger_num=$(TRIGGERS)
	ifeq ($(XLEN), 64)
		override define_macros += -D mcontext=0 -D scontext=0
	else
		override define_macros += -D mcontext=0 -D scontext=0
	endif
endif

ifneq ($(COUNTERS), 0)
	override define_macros += -D perfmonitors=True -D counters=$(COUNTERS)
endif

ifeq ($(COVERAGE), none)
else ifeq ($(COVERAGE),all)
  coverage := --coverage
else
  coverage := --coverage-$(COVERAGE)
endif

ifeq ($(TRACE), enable)
  trace := --trace
endif

ifeq ($(VERILATESIM), fast)
	verilate_fast := -CFLAGS -O3
endif

VCS_MACROS =  +define+BSV_RESET_FIFO_HEAD=True +define+BSV_RESET_FIFO_ARRAY=True

ifneq (0,$(VERBOSITY))
	VERILATOR_FLAGS += -DVERBOSE
	VCS_MACROS += +define+VERBOSE=True
endif

override define_macros += -D VERBOSITY=$(VERBOSITY) -D CORE_$(COREFABRIC)=True \
													-D MULSTAGES=$(MULSTAGES) -D DIVSTAGES=$(DIVSTAGES) \
													-D Counters=$(COUNTERS) -D paddr=$(PADDR) -D vaddr=$(XLEN) \
													-D PMPSIZE=$(PMPSIZE) -D resetpc=$(RESETPC) -D causesize=$(CAUSESIZE) \
													-D DTVEC_BASE=$(DTVEC_BASE) -D num_of_ops=$(NUM_OF_OPS) -D scop_user=$(SCOP_USER)

# ----------------------------------------------------------------------------------------------- #
# ------------------ Include directories for bsv compilation ------------------------------------ #
CORE:=./e-class/src/core/
M_EXT:=./e-class/src/core/m_ext/
FABRIC:=./fabrics/axi4:./fabrics/axi4lite
BMU:= ./bmu/src
CACHES:=./caches_mmu/src/
PERIPHERALS:=./devices/bootrom:./devices/uart:./devices/clint:./devices/bram:./devices/riscvDebug013:./devices/jtagdtm/:./devices/err_slave/:./devices/gpio/:./devices/plic/
COMMON_BSV:=./common_bsv/
COMMON_VERILOG:=./common_verilog/
BSVINCDIR:=.:%/Prelude:%/Libraries:%/Libraries/BlueNoC:$(CORE):$(BMU):$(M_EXT):$(FABRIC):$(PERIPHERALS):$(COMMON_BSV):$(COMMON_VERILOG)
# ----------------------------------------------------------------------------------------------- #

# ----------------- Setting up flags for verilator ---------------------------------------------- #
VERILATOR_FLAGS += --stats -O3 $(verilate_fast) -LDFLAGS "-static" --x-assign fast \
									 --x-initial fast --noassert sim_main.cpp --bbox-sys -Wno-STMTDLY \
									 -Wno-UNOPTFLAT -Wno-WIDTH -Wno-lint -Wno-COMBDLY -Wno-INITIALDLY \
									 --autoflush $(coverage) $(trace) --threads $(THREADS) \
									 -DBSV_RESET_FIFO_HEAD -DBSV_RESET_FIFO_ARRAY
# ---------------------------------------------------------------------------------------------- #

# ---------------- Setting the variables for bluespec compile  --------------------------------- #
BSC_CMD:= bsc -u -verilog -elab
BSVCOMPILEOPTS:= +RTS -K40000M -RTS -check-assert  -keep-fires -opt-undetermined-vals \
								 -remove-false-rules -remove-empty-rules -remove-starved-rules -remove-dollar \
								 -suppress-warnings G0020 -show-schedule -show-module-use
BSVLINKOPTS:=-parallel-sim-link 8 -keep-fires
VERILOGDIR:=./verilog/
VRLG_ARTFCTS:= ./verilog-artifacts/bSoC_$(XLEN)
BSVBUILDDIR:=./bsv_build/
BSVOUTDIR:=./bin
ifeq (, $(wildcard ${TOOLS_DIR}/shakti-tools/insert_license.sh))
  VERILOG_FILTER:= -verilog-filter ${BLUESPECDIR}/bin/basicinout
else
  VERILOG_FILTER:= -verilog-filter ${BLUESPECDIR}/bin/basicinout \
									 -verilog-filter ${TOOLS_DIR}/shakti-tools/insert_license.sh \
									 -verilog-filter ./rename_translate.sh
	VERILOGLICENSE:= cp ${TOOLS_DIR}/shakti-tools/IITM_LICENSE.txt ${VERILOGDIR}/LICENSE.txt
endif
# ----------------------------------------------------------------------------------------------- #

# ---------------- Setting variables for Benchmark compilation  --------------------------------- #
BARE_ARCH ?= rv${XLEN}imac
ARCH ?= ${BARE_ARCH}b
ABI ?= ilp${XLEN}
CPU_MHZ ?= 1
WARMUP_HEAT=0
RISCV_PREFIX ?= riscv32-unknown-elf-
RISCV_GCC ?= ${RISCV_PREFIX}gcc
RISCV_LINK_OPTS ?= -static -nostartfiles -nostdlib -ffast-math -fno-common -std=gnu99 -Os -fno-builtin-printf \
-fno-exceptions  -fdata-sections -ffunction-sections -Wl,-gc-sections -fno-inline -fgcse-las # -msave-restore -ffreestanding -fno-builtin
RISCV_HEX ?= elf2hex $$(($(XLEN)/8)) 8388608
RISCV_OBJDUMP ?= $(RISCV_PREFIX)objdump -D -Mnumeric
REFDIR ?= $(shell readlink -f ./tests/workdir)
EMBENCH_SPRT_FILES ?= $(shell readlink -e ./tests/benchmarks/support/)
# BMRK_SPRT_FILE ?= $(shell readlink -e ./benchmarks/common/)
HDR_FILES ?= -I$(EMBENCH_SPRT_FILES) -T$(EMBENCH_SPRT_FILES)/link.ld
SPRT_FILES ?= $(EMBENCH_SPRT_FILES)/syscalls.c $(EMBENCH_SPRT_FILES)/crt.S $(EMBENCH_SPRT_FILES)/dummy-libgcc.c \
$(EMBENCH_SPRT_FILES)/main.c $(EMBENCH_SPRT_FILES)/dummy-libm.c $(EMBENCH_SPRT_FILES)/dummy-libc.c \
$(EMBENCH_SPRT_FILES)/beebsc.c

# ---------------- PYTHON CHECK FROM MAKEFILE  --------------------------------- #
PYTHON := $(shell which python3)
ifeq (, $(shell which python ))
  $(error "PYTHON=$(PYTHON) not found in $(PATH)")
endif

PYTHON_VERSION=$(shell $(PYTHON) -c 'import sys; print("%d.%d"% sys.version_info[0:2])' )
PYTHON_VERSION_OK=$(shell $(PYTHON) -c 'import sys;  print(int(float("%d.%d"% sys.version_info[0:2]) >= $(PYTHON_VERSION_MIN)))' )

RISCOF_DIR := ./riscof/
RISCV_CONFG:= ./riscv-config/
RISCV_PLUGIN:= ./tests/riscof/riscof-plugins/

# ------------------------------------- Makefile TARGETS ---------------------------------------- #

default: help
gdb: generate_verilog link_verilator_gdb generate_boot_files

.PHONY: help
help: # This help dialog.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//' | column	-c2 -t -s :

check-env:
	@if test -z "$(BLUESPECDIR)"; then echo "BLUESPECDIR variable not set"; exit 1; fi;

check-pythn:
	@if test $(PYTHON_VERSION_OK) -eq 0 ; then echo "RISCOF compliance Needs python >= $(PYTHON_VERSION_MIN)"; exit 1; fi;

check-fpga:
ifneq ($(SYN),FPGA)
	$(error provide SYN option, use "make synthesis SYN=FPGA")
endif

.PHONY: check-restore
check-restore:
	@if [ "$(define_macros)" != "$(old_define_macros)" ];	then	make clean ;	fi;

.PHONY: update_xlen
update_xlen:
	@echo "XLEN=$(XLEN)" > $(TOP_DIR)/verification/dts/Makefile.inc

.PHONY: call_manager
call_manager:
	@/bin/bash ./manager.sh update_deps

.PHONY: set_jtag_bscan2e
set_jtag_bscan2e:
	@sed -i 's/BSCAN2E=.*/BSCAN2E=enable/g' ./support/soc_synthesis_config.inc

.PHONY: set_raw_jtag_jlink
set_raw_jtag_jlink:
	@sed -i 's/BSCAN2E=.*/BSCAN2E=disable/g' ./support/soc_synthesis_config.inc

.PHONY: simulate
simulate: ## Simulate the bSoC on Verilator
simulate:call_manager generate_verilog link_verilator generate_boot_files

.PHONY: synthesis
synthesis: ## Synthesize bSoC on ARTY Board using BSCAN2E
synthesis: check-fpga clean_fpga call_manager set_jtag_bscan2e generate_verilog generate_boot_code ip_build arty_build generate_mcs program_mcs_spansion

.PHONY: synthesis_jtag
synthesis_jtag: ## synthesize bSoC on ARTY board using JTAG interface
synthesis_jtag: call_manager set_raw_jtag_jlink generate_verilog ip_build arty_build program


.PHONY: generate_verilog
generate_verilog: # Generete verilog from BSV
generate_verilog: check-restore check-env
	@echo Compiling $(TOP_MODULEendif) in verilog ...
	@mkdir -p $(BSVBUILDDIR);
	@mkdir -p $(VERILOGDIR);
	@echo "old_define_macros = $(define_macros)" > old_vars
	$(BSC_CMD) -vdir $(VERILOGDIR) -bdir $(BSVBUILDDIR) -info-dir $(BSVBUILDDIR)\
  $(define_macros) $(BSVCOMPILEOPTS) \
  -p $(BSVINCDIR) -g $(TOP_MODULE) $(TOP_DIR)/$(TOP_FILE)  || (echo "BSC COMPILE ERROR"; exit 1)
	@cp ${BLUESPECDIR}/Verilog.Vivado/RegFile.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog.Vivado/BRAM2BELoad.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog.Vivado/BRAM2BE.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog.Vivado/BRAM2.v ${VERILOGDIR}
	@cp $(TOP_DIR)/common_verilog/bram_1r1w.v ${VERILOGDIR}
	@cp $(TOP_DIR)/common_verilog/bram_1rw.v ${VERILOGDIR}
	@cp $(TOP_DIR)/common_verilog/BRAM1Load.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/FIFO2.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/FIFO1.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/FIFO10.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/RevertReg.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/FIFO20.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/FIFOL1.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/SyncFIFO.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/Counter.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/SizedFIFO.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/RegFileLoad.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/SyncReset0.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/SyncRegister.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/MakeClock.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/UngatedClockMux.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/ClockInverter.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/MakeResetA.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/MakeReset0.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/SyncResetA.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/ResetEither.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/SyncHandshake.v ${VERILOGDIR}
	@cp ${BLUESPECDIR}/Verilog/SyncFIFO1.v ${VERILOGDIR}
	@$(VERILOGLICENSE)
	@echo Compilation finished


.PHONY: link_verilator
link_verilator: # Generate simulation executable using Verilator
	@echo "Linking $(TOP_MODULE) using verilator"
	@mkdir -p $(BSVOUTDIR) $(TOP_DIR)/obj_dir
	@echo "#define TOPMODULE V$(TOP_MODULE)" > $(TOP_DIR)/sim_main.h
	@echo '#include "V$(TOP_MODULE).h"' >> $(TOP_DIR)/sim_main.h
	verilator $(VERILATOR_FLAGS) --cc $(TOP_MODULE).v -y ./verilog/ --exe
	@ln -f -s $(TOP_DIR)/sim_main.cpp $(TOP_DIR)/obj_dir/sim_main.cpp
	@ln -f -s $(TOP_DIR)/sim_main.h $(TOP_DIR)/obj_dir/sim_main.h
	@make -j8 -C $(TOP_DIR)/obj_dir -f V$(TOP_MODULE).mk
	@cp $(TOP_DIR)/obj_dir/V$(TOP_MODULE) $(BSVOUTDIR)/out


.PHONY: link_verilator_gdb
link_verilator_gdb: # Generate simulation executable using Verilator and VPI for GDB
	@echo "Linking Verilator With the Shakti RBB Vpi"
	@mkdir -p $(TOP_DIR)/bin
	@echo "#define TOPMODULE V$(TOP_MODULE)_edited" >$(TOP_DIR)/sim_main.h
	@echo '#include "V$(TOP_MODULE)_edited.h"' >> $(TOP_DIR)/sim_main.h
	@sed  -f $(TOP_DIR)/devices/jtagdtm/sed_script.txt  $(VERILOGDIR)/$(TOP_MODULE).v > $(TOP_DIR)/tmp1.v
	@cat  $(TOP_DIR)/devices/jtagdtm/verilator_config.vlt \
	      $(TOP_DIR)/devices/jtagdtm/vpi_sv.v \
	      $(TOP_DIR)/tmp1.v                         > $(VERILOGDIR)/$(TOP_MODULE)_edited.v
	@rm   -f  $(TOP_DIR)/tmp1.v
	verilator --threads-dpi all --cc $(TOP_MODULE)_edited.v --exe sim_main.cpp \
						devices/jtagdtm/RBB_Shakti.c -y $(VERILOGDIR) $(VERILATOR_FLAGS)
	@ln -f -s $(TOP_DIR)/sim_main.cpp $(TOP_DIR)/obj_dir/sim_main.cpp
	@ln -f -s $(TOP_DIR)/sim_main.h $(TOP_DIR)/obj_dir/sim_main.h
	@ln -f -s $(TOP_DIR)/devices/jtagdtm/RBB_Shakti.c $(TOP_DIR)/obj_dir/RBB_Shakti.c
	@echo "INFO: Linking verilated files"
	@make -j8 -C $(TOP_DIR)/obj_dir -f V$(TOP_MODULE)_edited.mk
	@cp $(TOP_DIR)/obj_dir/V$(TOP_MODULE)_edited $(TOP_DIR)/bin/out
	@cp e-class/base-sim/gdb_setup/code.mem* $(TOP_DIR)/bin/
	@echo Linking finished

.PHONY: release-verilog-artifacts
release-verilog-artifacts: # target to generate verilog artifacts
release-verilog-artifacts: generate_verilog generate_boot_code #link_verilator
	@rm -rf $(TOP_DIR)/$(VRLG_ARTFCTS)
	@mkdir -p $(TOP_DIR)/$(VRLG_ARTFCTS)
	@mkdir -p $(TOP_DIR)/$(VRLG_ARTFCTS)/sim
	@cp -r ${VERILOGDIR} $(TOP_DIR)/$(VRLG_ARTFCTS)/
	@cp ${CONFIG} $(TOP_DIR)/$(VRLG_ARTFCTS)/
	@cp ./LICENSE $(TOP_DIR)/$(VRLG_ARTFCTS)/LICENSE

.PHONY: regress
regress: # To run regressions on the core.
	@DIR_HOME=$$PWD perl -I$(DIR_HOME)/verification/verif-scripts $(DIR_HOME)/verification/verif-scripts/makeRegress.pl $(opts)

.PHONY: test
test: # To run a single riscv-test on the core.
	@DIR_HOME=$$PWD CONFIG_LOG=0 perl -I$(DIR_HOME)/verification/verif-scripts $(DIR_HOME)/verification/verif-scripts/makeTest.pl $(opts)

.PHONY: torture
torture: # To run riscv-tortur on the core.
	@DIR_HOME=$$PWD perl -I$(DIR_HOME)/verification/verif-scripts $(DIR_HOME)/verification/verif-scripts/makeTorture.pl $(opts)

.PHONY: aapg
aapg: # to generate and run aapf tests
	@DIR_HOME=$$PWD perl -I$(DIR_HOME)/verification/verif-scripts $(DIR_HOME)/verification/verif-scripts/makeAapg.pl $(opts)

.PHONY: csmith
csmith: # to generate and run csmith tests
	@DIR_HOME=$$PWD perl -I$(DIR_HOME)/verification/verif-scripts $(DIR_HOME)/verification/verif-scripts/makeCSmith.pl $(opts)

.PHONY: generate_boot_code
generate_boot_code: # to generate boot files for synthesis
generate_boot_code: update_xlen
	@cp support/boot-code/* $(VERILOGDIR)/


.PHONY: generate_boot_files
generate_boot_files: # to generate boot files for simulation
generate_boot_files: update_xlen
	@mkdir -p $(BSVOUTDIR)
	@cd $(TOP_DIR)/verification/dts/; make;
	@cp $(TOP_DIR)/verification/dts/boot.hex  ${VERILOGDIR}/boot.mem
	@cut -c1-8 $(TOP_DIR)/verification/dts/boot.hex > bin/boot.MSB
	@if [ "$(XLEN)" = "64" ]; then\
	  cut -c9-16 $(TOP_DIR)/verification/dts/boot.hex > bin/boot.LSB;\
    else cp bin/boot.MSB bin/boot.LSB;\
  fi


.PHONY: patch
patch:
	@cd $(DIR_HOME)/verification/riscv-tests/env && git apply $(DIR_HOME)/verification/patches/riscv-tests-shakti-signature.patch

.PHONY: unpatch
unpatch:
	@cd $(DIR_HOME)/verification/riscv-tests/env && git apply -R $(DIR_HOME)/verification/patches/riscv-tests-shakti-signature.patch

.PHONY: yml
yml:
	@DIR_HOME=$$PWD python3 $(DIR_HOME)/verification/verif-scripts/gen_yml.py $(opts)

.PHONY: ip_build
ip_build: # build Xilinx Core-IPs used in this project
	vivado -log ipbuild.log -nojournal -mode tcl -notrace -source support/tcl/create_ip_project.tcl \
		-tclargs $(FPGA) $(XLEN) $(MULSTAGES) $(ISA) $(JOBS) \
		|| (echo "Could not create IP project"; exit 1)

.PHONY: arty_build
arty_build:
	vivado -nojournal -nolog -mode tcl -notrace -source support/tcl/create_project.tcl -tclargs $(SYNTHTOP) $(FPGA) $(ISA) $(JTAG_TYPE) \
	|| (echo "Could not create core project"; exit 1)
	vivado -nojournal -log artybuild.log -notrace -mode tcl -source support/tcl/run.tcl \
		-tclargs $(JOBS) || (echo "ERROR: While running synthesis")

.PHONY: program_fpga_jlink
program_fpga_jlink: # program the fpga
	sudo $(Vivado_loc) -nojournal -nolog -mode tcl -source support/tcl/program.tcl

.PHONY: generate_mcs
generate_mcs: # Generate the FPGA Configuration Memory file.
	vivado -nojournal -nolog -mode tcl -source support/tcl/generate_mcs.tcl

.PHONY: program_mcs_spansion
program_mcs_spansion: #Program the FPGA Configuration Memory in order to use the onboard ftdi jtag chain
	sudo $(Vivado_loc) -nojournal -nolog -mode tcl -source support/tcl/program_mcs_arty_a7_spansion.tcl
	echo "Please Disconnect and reconnect Your Arty Board from your PC"
	echo "After programming reset the device once and run \"sudo openocd \
	-f support/bsoc-arty.cfg\" to start a gdb server at localhost:3333 "

.PHONY: program_mcs_micron
program_mcs_micron: # Program the FPGA Configuration Memory in order to use the onboard ftdi jtag chain
	sudo $(Vivado_loc) -nojournal -nolog -mode tcl -source support/tcl/program_mcs_arty_a7_micron.tcl
	echo "Please Disconnect and reconnect Your Arty Board from your PC"
	echo "After programming reset the device once and run \"sudo openocd \
	-f support/bsoc-arty.cfg\" to start a gdb server at localhost:3333 "


.PHONY: check-exc
check-exc:
	@if test ! -f  "./bin/out" ; then printf "Executable Not Found \n Use command \'make default\' \n"; exit 1; fi;

setup-riscof-env:
	@cd $(RISCV_CONFG); pip3 install -r requirements.txt;
	@cd $(RISCOF_DIR) ; pip3 install -r requirements.txt ;

.PHONY: compliance
compliance: check-pythn remove-ebreak
# 	@cd $(RISCOF_DIR) ; python3.7 -m riscof.main -v ;
	@echo "Starting Compliance Test -  RV32IMB"
	@$(eval export OUTFILE = $(shell readlink -e $(BSVOUTDIR)))
	@$(eval export PYTHONPATH=$(shell readlink -e $(RISCOF_DIR)):$(shell readlink -e $(RISCV_CONFG)):$(shell readlink -e $(RISCV_PLUGIN)/eClass-Bitmanip):$(shell readlink -e $(RISCV_PLUGIN)/spike_parallel))
	@echo $(PYTHONPATH)
	@cd $(RISCOF_DIR) ; cp -fuv ../tests/riscof/riscof/config.ini ./ ;
	@sed -i "s|eClassBin=../bin/|eClassBin=$${OUTFILE}|" ./riscof/config.ini ;
	@cd $(RISCV_CONFG); sed -i 's/ACDEFG/ABCDEFG/' ./riscv_config/schemas/schema_isa.yaml;
	@cd $(RISCV_CONFG); sed -i 's/ACDEFG/ABCDEFG/' ./riscv_config/schemaValidator.py;
	@cd $(RISCOF_DIR) ; python3.7 -m riscof.main --config=config.ini --validateyaml ;
# 	@cd $(RISCOF_DIR) ; cp -furv ../tests/riscof/riscof/suite/rv32i_m_b/* ./riscof/suite/rv32i_m/ ;
	@cd $(RISCOF_DIR) ; git add ./riscof/suite/rv32i_m/ &&  git diff-index --cached --quiet HEAD || git commit --amend -m '' --allow-empty-message ; python3.7 -m riscof.dbgen --verbose=debug;
# 	@cd $(RISCOF_DIR) ; python3.7 -m riscof.main --config=config.ini --testlist ;
	@cd $(RISCOF_DIR) ; sed -i 's|ascii|utf-8|' ./riscof/utils.py ;
	@cd $(RISCOF_DIR) ; time python3.7 -m riscof.main --config=config.ini --run ;

.PHONY: remove-ebreak
remove-ebreak:
	@cd $(RISCOF_DIR) ; rm -f ./riscof/suite/rv32i_m/I/I-EBREAK-01.S
	@cd $(RISCOF_DIR) ; rm -f ./riscof/suite/rv32i_m/privilege/I-EBREAK-01.S

.PHONY: clean
clean: ## clean entire repository
clean: clean_artifacts clean_verif clean fpga clean_verilog
	@/bin/bash ./manager.sh nuke

.PHONY: clean_verilog
clean_verilog: ## clean generated Verilog portions
clean_verilog: clean
	rm -rf $(TOP_DIR)/verilog/
	rm -rf fpga/
	rm -rf INCA*
	rm -rf work
	rm -rf ./ncvlog.*
	rm -rf irun.*

clean_fpga: ## delete fpga_prject and journal/log files as well
	sudo rm -rf fpga_project *.jou *.log .Xil

clean_verif: ## clean verification portions of bSoC
	rm -rf $(TOP_DIR)/verification/workdir/*
	rm -rf $(TOP_DIR)/verification/riscv-torture/output/riscv-torture
	rm -rf $(TOP_DIR)/riscof/riscof_work/*

clean_artifacts:
	@rm -rf $(TOP_DIR)/verilog-artifacts/*

restore: clean_verilog
