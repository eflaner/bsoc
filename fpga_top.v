/*
Copyright (c) 2018, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and/or other materials provided
 with the distribution.
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Arjun Menon , P.George
Details:

--------------------------------------------------------------------------------------------------
*/
module fpga_top(
  // ---- JTAG ports ------- //
  `ifndef JTAG_BSCAN2E
    input       pin_tck,
    input       pin_trst,
    input       pin_tms,
    input       pin_tdi,
    output      pin_tdo,
  `endif

   // ---- UART ports --------//
   input         uart_SIN,
   output        uart_SOUT,

   // ---- GPIO ports --------//
   inout[7:0] gpio,

   // ---- External interrupts ports --------//
//   input [1:0] interrupts,

   // ---- System Reset ------//
   input         sys_rst,	//Active Low
   output        locked_glow,

   // ---- System Clock ------//
   input         sys_clk
   );



   // Wire instantiations

   wire                            soc_reset;      // reset to the SoC
   wire                            core_clk;       // clock to the SoC
   wire                            locked;         // indicates pll is stable
   wire                            mmcm_locked;    // indicates the ui clock from mig is stable
   wire [7:0] gpio_in, gpio_out, gpio_out_en;

   // ---------------------------------------------------------------------------- //


   // ---------------------------------------------------------------------------- //
  `ifdef JTAG_BSCAN2E
    wire wire_tck_clk;
    wire wire_trst;
    wire wire_capture;
    wire wire_run_test;
    wire wire_sel;
    wire wire_shift;
    wire wire_tdi;
    wire wire_tms;
    wire wire_update;
    wire wire_tdo;

    BSCANE2 #(
      .JTAG_CHAIN(4) // Value for USER command.
    )
    bse2_inst (
      .CAPTURE(wire_capture), // 1-bit output: CAPTURE output from TAP controller.
      .DRCK(), // 1-bit output: Gated TCK output. When SEL is asserted, DRCK toggles when CAPTURE or SHIFT are asserted.
      .RESET(wire_trst), // 1-bit output: Reset output for TAP controller.
      .RUNTEST(wire_run_test), // 1-bit output: Output asserted when TAP controller is in Run Test/Idle state.
      .SEL(wire_sel), // 1-bit output: USER instruction active output.
      .SHIFT(wire_shift), // 1-bit output: SHIFT output from TAP controller.
      .TCK(wire_tck_clk), // 1-bit output: Test Clock output. Fabric connection to TAP Clock pin.
      .TDI(wire_tdi), // 1-bit output: Test Data Input (TDI) output from TAP controller.
      .TMS(wire_tms), // 1-bit output: Test Mode Select output. Fabric connection to TAP.
      .UPDATE(wire_update), // 1-bit output: UPDATE output from TAP controller
      .TDO(wire_tdo) // 1-bit input: Test Data Output (TDO) input for USER function.
    );
  `endif

   assign soc_reset = locked ;
   assign locked_glow = locked;
   // ---------------------------------------------------------------------------- //

   // ---------- Clock divider ----------------//

   clk_divider clk_div (
                       .clk_in1(sys_clk),
                       .clk_out1(core_clk),
                       .resetn(sys_rst),
                       .locked(locked) );
   // ----------------------------------------- //


   // ---- Instantiating the C-class SoC -------------//
   mkSoc core(
       // Main Clock and Reset to the SoC
       .CLK(core_clk),
       .RST_N(soc_reset),

`ifndef JTAG_BSCAN2E
       // JTAG port definitions
       .CLK_tck_clk(pin_tck),
       .RST_N_trst(pin_trst),
       .wire_tms_tms_in(pin_tms),
       .wire_tdi_tdi_in(pin_tdi),
       .wire_tdo(pin_tdo),
`else
        .CLK_tck_clk(wire_tck_clk),
        .RST_N_trst(~wire_trst),
        .wire_capture_capture_in(wire_capture),
        .wire_run_test_run_test_in(wire_run_test),
        .wire_sel_sel_in(wire_sel),
        .wire_shift_shift_in(wire_shift),
        .wire_tdi_tdi_in(wire_tdi),
        .wire_tms_tms_in(wire_tms),
        .wire_update_update_in(wire_update),
        .wire_tdo(wire_tdo),
`endif
       // UART port definitions
       .uart_io_SIN(uart_SIN),
       .uart_io_SOUT(uart_SOUT),

			 //External interrupts ports
//       .interrupts_inp(interrupts),

			 //GPIO ports
       .gpio_io_gpio_in_inp(gpio_in),
       .gpio_io_gpio_out(gpio_out),
       .gpio_io_gpio_out_en(gpio_out_en)
   );

   // ---- Instantiating the C-class SoC -------------//
   genvar index;
   generate
   for(index=0; index<8; index= index+1)
      begin: connect_gpio_tristates
      IOBUF gpio_iobuf_inst (
             .O(gpio_in[index]),
             .IO(gpio[index]),
             .I(gpio_out[index]),
             .T(~gpio_out_en[index])
				 );
      end
   endgenerate

// Enable if onboard flash or disable if external flash
   STARTUPE2#(.PROG_USR("False"),
               .SIM_CCLK_FREQ(0.0))   startupe2_inst1(
               .CFGCLK(open),
               .CFGMCLK(open),
               .EOS(open),
               .PREQ(open),
               .CLK(0),
               .GSR(0),
               .GTS(0),
               .KEYCLEARB(0),
               .PACK(0),
               .USRCCLKO(spi0_sclk),
               .USRCCLKTS(0),
               .USRDONEO(1),
               .USRDONETS(1));

endmodule
