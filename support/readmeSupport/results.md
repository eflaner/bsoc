# Benchmarking bSoC using Embench<sup>TM</sup>

bSoC is compared against the standard SoC  (i.e., without the BMU) for evaluation the impact of BMI using benchmark Embench<sup>TM</sup> 

## Embench Record of Details

- Embench Version :=  0.5
- Platform := Cycle Accurate Verilator Simulated
- Operated Frequency := 1 MHz
- Toolchain:

|   Risc-V Toolchain    | 32-BIT   |
| :-------------------: | -------- |
| riscv-binutils 2.33.5 | c8704188 |
|   riscv-gcc 10.0.0    | d69e3efd |
|   riscv-newlib-3.2    | f289cef6 |

- Compiler Options:

```text
-mabi=ilp32, -mcmodel=medany, -c,-fdata-sections, -ffunction-sections -Wl,-gc-sections, -nostartfiles, -nostdlib
```

- |    Standard Soc |       bSoc       |
  | --------------: | :--------------: |
  | -march=rv32imac | -march=rv32imacb |


- | Varying options    |      |      |      |      |
  | ------------------ | ---- | ---- | ---- | ---- |
  | -Os -msave-restore | -Os  | -O1  | -O2  | -O3  |

## Embench Scores

The Embench score for size in absolute numbers. The number at the tip represents the error-bar in numerical format.

![Embench Size Score](./Embench Size Score.png)

The Embench Speed score for size in absolute numbers.

![Embench Speed Score](./Embench Speed Score.png)

while running for speed, the elf files are checked for size whose size score plot is shown below.

![Absolute Embench Score](./Embench Size Score Abs.png)

The CPI chart is built while running for speed 

![CPI Sheet](./CPI Sheet.png)

## Data set

### BMI Hotspot :fire:

BMI found in each benchmark

<img src="./BMI Hotspot.png" alt="BMI Hotspot" style="zoom:80%;" />

### Speed :running:

![Speed dataset](./Speed Table.png)

### CPI Table :bar_chart:

![Size dataset](./CPI Table.png)

### Size Dataset (ELF  calculated when testing for speed) :page_with_curl:

![Absolute Speed size](./Abs Size Table.png)

### Size Dataset( using dummy libraries as specified by Embench) :page_facing_up:

![Rel Size](./Rel Size Table.png)

## Note:

*The results might vary with change in compilers and usage of tools and should be considered with caution. Embench is used to derive the results using  [bc7a9d6bf399dd5a90a613bb313c078784f4847d](https://github.com/embench/embench-iot/tree/master)r