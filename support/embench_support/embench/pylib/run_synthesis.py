#!/usr/bin/env python3

# Python module to run programs natively.

# Copyright (C) 2020 Babu P S
#
# Contributor: Babu P S <babu.ps@hotmail.com>
#
# This file is part of Embench.

# SPDX-License-Identifier: GPL-3.0-or-later

"""
Embench module to run benchmark programs.

This version is suitable for running programs on shakti core
"""

__all__ = [
    'get_target_args',
    'build_benchmark_cmd',
    'decode_results',
]

import argparse
import subprocess
import re
import fileinput

from os import path
from tabulate import tabulate
from embench_core import log
from collections import Counter

cpu_mhz = 50

def get_target_args(remnant):
    """Parse left over arguments"""
    parser = argparse.ArgumentParser(description='Get target specific args')
    parser.add_argument(
        '--gdb-command',
        type=str,
        default='riscv32-unknown-elf-gdb',
        help='Command to invoke GDB',
    )
    parser.add_argument(
        '--gdbserver-command',
        type=str,
        default='-x /gdb.script',
        help='Command to invoke the GDB server',
    )
    parser.add_argument(
        '--cpu-mhz',
        type=int,
        default=50,
        help='Processor clock speed in MHz'
    )
    # No target arguments
    return parser.parse_args(remnant)

def displayresults(table):
    headers = ['Metric', 'Value']
    return tabulate(table, headers, tablefmt="psql", numalign="right",stralign="right",disable_numparse=True)

def build_benchmark_cmd(bench, benchdir, args):
    """Construct the command to run the benchmark.  "args" is a
       namespace with target specific arguments"""
    global cpu_mhz
    cpu_mhz = args.cpu_mhz
    cmd = [f'{args.gdb_command}']
    gdb_comms = [
        'set confirm off',
        'set style enabled off',
        'set height 0',
        'set remotetimeout unlimited',
        'target remote localhost:3333',
        'monitor reset halt',
        'monitor gdb_sync',
        'file {0}',
        'load',
        'delete breakpoints',
        'break xit', #defined at the exit
        'jump _start',
        # 'continue',
        # 'p {char* [4]}&counter_names',
        'print {{unsigned long long [4]}}&counters',
        'detach',
        'quit'
    ]

    for arg in gdb_comms:
        cmd.extend(['-ex', arg.format(bench)])

    return cmd

def decode_results(bench, appdir, stdout_str, stderr_str):
    """Extract the results from the output string of the run. Return the
       elapsed time in milliseconds or zero if the run failed."""
    # See above in build_benchmark_cmd how we record the return value and
    # execution time. Return code is in standard output. Execution time is in
    # standard error.

    times = re.search('\$1 = \D+(\d+)\D+(\d+)\D+(\d+)\D+(\d+)', stdout_str, re.S)
    metrics = dict()
    metrics['bench_name'] = str(bench)
    metrics['time_report'] = int(times.group(1) )
    metrics['cycle_count'] = int(times.group(2) )
    metrics['instr_count'] = int(times.group(3) )
    metrics['result_code'] = int(times.group(4) )

    if metrics['result_code']:
        log.warning(f"Warning: Failed with return code {metrics['result_code']}")
        return 0.0

    global cpu_mhz
    metrics['CPI'] = float( metrics['cycle_count']/metrics['instr_count'])
    metrics['freq'] = int(cpu_mhz)
    metrics['exec_freq'] = float( metrics['cycle_count']/metrics['time_report'])
    metrics['cpu_time'] = (float(metrics['instr_count']*metrics['CPI']*(0.000001/metrics['freq'])))
    # log.info('\nCPI = '+str(metrics['CPI'])+' cycles/instruction')
    # log.info('\nfreq = '+str(metrics['freq'])+' MHz')
    # log.info('\nExecuting freq = '+str(metrics['exec_freq'])+' MHz')
    # log.info('\ncpu_time = '+str(metrics['cpu_time'])+' seconds \n')
    log.info('\n\n\t SUMMARY\n'+displayresults(list(metrics.items()))+'\n')


#time can be measured in three ways
    # 1. using cycles and cpu_mhz (highly accurate)
    # 2. using time csr of riscv  (yet to verify)
    # 3. using time output of shell (most inaccurate)

    if metrics['cycle_count']:
        ms_elapsed = (metrics['cycle_count'] / cpu_mhz) * 0.001 ; #in milliseconds

        # ms_elapsed = metrics['time_report']

        # time = re.search('^real (\d+)[.](\d+)', stdout_str, re.S)
        # ms_elapsed = int(time.group(1)) * 1000 + \
        #              int('{:<03d}'.format(int(time.group(2)))) # 0-pad

        # Return value cannot be zero (will be interpreted as error)
        return max(float(ms_elapsed), 0.001)
    else:
        # We must have failed to find a time
        log.warning('Warning: Failed to find timing')
        return 0.0
