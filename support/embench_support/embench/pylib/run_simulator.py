#!/usr/bin/env python3

# Python module to run programs natively.

# Copyright (C) 2020 Babu P S
#
# Contributor: Babu P S <babu.ps@hotmail.com>
#
# This file is part of Embench.

# SPDX-License-Identifier: MIT

"""
Embench module to run benchmark programs.

This version is suitable for running programs on bSoC simulated executable
"""

__all__ = [
    'get_target_args',
    'build_benchmark_cmd',
    'decode_results',
]

import argparse
import subprocess
import re
import fileinput\

from os import path
from tabulate import tabulate
from embench_core import log
from collections import Counter

cpu_mhz = 1

instrn_list = ['andn', 'orn', 'xnor','slo','sro','rol','ror', 'sh1add','packu',\
'sh2add', 'sh3add', 'sbclr', 'sbset', 'sbinv', 'sbext', 'gorc', 'grev', 'sloi',\
'rori', 'sbclri', 'sbseti', 'sbexti', 'sbinvi', 'gorci','grevi', 'cmix','cmov',\
'sroi','fsl', 'fsr', 'fsri', 'clz','ctz', 'pcnt', 'sext.b','sext.h', 'crc32.b',\
'crc32.h','crc32.w','crc32c.b','crc32c.h', 'crc32c.w', 'clmul', 'clmulr','min',\
'clmulh',  'max', 'minu', 'maxu', 'shfl','unshfl','bdep', 'bext', 'pack','bfp',\
'packh', 'shfli', 'unshfli']

def get_target_args(remnant):
    """Parse left over arguments"""
    parser = argparse.ArgumentParser(description='Get target specific args')

    # No target arguments
    return parser.parse_args(remnant)

def displayresults(table):
    headers = ['Metric', 'Value']
    return tabulate(table, headers, tablefmt="psql", numalign="right",stralign="right",disable_numparse=True)

def build_benchmark_cmd(bench, benchdir, args):
    """Construct the command to run the benchmark.  "args" is a
       namespace with target specific arguments"""

    # Due to way the target interface currently works we need to construct
    # a command that records both the return value and execution time to
    # stdin/stdout. Obviously using time will not be very precise.
    executabl = '<path to verilator executabl>/bin'
    try:
        subprocess.check_call("riscv32-unknown-elf-objdump -d -Mnumeric "+bench+" > "+bench+".disass", shell=True, cwd=benchdir)
        subprocess.check_call("elf2hex 4 8388608 "+bench+" 2147483648 > code.mem", shell=True, cwd=benchdir)
        binout = subprocess.check_output("readlink -f "+executabl, shell=True, cwd=benchdir).decode('utf-8').strip()
        subprocess.check_call("ln -sf "+binout+'/* ./', shell=True, cwd=benchdir)
    except Exception as e:
        raise e
    return ['sh', '-c', 'time -p ./out | tee ./'+bench+'.log; echo RE T=$?']
    # return ['sh', '-c', 'time -p ./' + bench + '; echo RE T=$?']


def decode_results(bench, appdir, stdout_str, stderr_str):
    """Extract the results from the output string of the run. Return the
       elapsed time in milliseconds or zero if the run failed."""
    # See above in build_benchmark_cmd how we record the return value and
    # execution time. Return code is in standard output. Execution time is in
    # standard error.

    # Match "RET=rc"
    metrics = dict()
    ins_list = list()
    ins_dis_list = list()
    file_name = appdir+str('/app_log')
    if path.isfile(file_name):
        subprocess.check_call("rm -rf code.mem rtl.dump", shell=True, cwd=appdir)
        metrics['bench_name'] = str(bench)
        for line in fileinput.input(file_name, inplace=True):
            if line.lstrip().startswith("result = "):
                metrics['result_code'] = int( re.search('result = (\d+)', line.strip(), re.S).group(1) )
            if line.lstrip().startswith("mcycle = "):
                metrics['cycle_count'] = int(re.search('mcycle = (\d+)', line.strip(), re.S).group(1))
            if line.lstrip().startswith("minstret = "):
                metrics['instr_count'] = int(re.search('minstret = (\d+)', line.strip(), re.S).group(1))
            if line.lstrip().startswith("Time = "):
                metrics['time_report'] = int(re.search('Time = (\d+)', line.strip(), re.S).group(1))
            print(line, end='')

    logfile = appdir+"/"+bench+".log"
    if path.isfile(logfile):
        accumulator = []
        for line in fileinput.input(logfile, inplace=True):
            if line.lstrip().startswith("SCOP"):
                search_term = line.split("\t")[1]
                check_term = line.split("\t")[2]
                pseudoynm = read_from_assembly(bench,search_term,check_term,appdir)
                accumulator.append(pseudoynm[0])
                line = line.replace(line,'\t'.join(line.split("\t")[1:3]+pseudoynm+line.split("\t")[3:]))
            print(line, end='')
        cnt = Counter(accumulator)
        ins_list = cnt.most_common()
        ins_list.append(tuple(['=== Total ===',sum(cnt.values())]))
        ins_dis_list = read_from_disassembly(bench,appdir)

    if (('instr_count' in metrics) and ('cycle_count' in metrics)):
        global cpu_mhz
        metrics['CPI'] = float( metrics['cycle_count']/metrics['instr_count'])
        metrics['freq'] = int(cpu_mhz)
        metrics['exec_freq'] = float( metrics['cycle_count']/metrics['time_report'])
        metrics['cpu_time'] = (float(metrics['instr_count']*metrics['CPI']*(0.000001/metrics['freq'])))
        with open(file_name, "a") as myfile:
            myfile.write('\nCPI = '+str(metrics['CPI'])+' cycles/instruction')
            myfile.write('\nfreq = '+str(metrics['freq'])+' MHz')
            myfile.write('\nExecuting freq = '+str(metrics['exec_freq'])+' MHz')
            myfile.write('\ncpu_time = '+str(metrics['cpu_time'])+' seconds \n')
            myfile.write('\n\n\t SUMMARY\n'+displayresults(list(metrics.items()))+'\n')
            myfile.write('\n\n\tOccurences in Trace       \n'+displayresults(ins_list))
            myfile.write('\n\n Occurences in disassembly \n'+displayresults(ins_dis_list))
    else:
        log.info('Warning: some keys were not found in dict')

    if not 'result_code' in metrics:
        log.warning('Warning: Failed to find return code')
        return 0.0

#time can be measured in three ways
    # 1. using cycles and cpu_mhz (highly accurate)
    # 2. using time csr of riscv  (yet to verify)
    # 3. using time output of shell (most inaccurate)

    if 'cycle_count' in metrics:
        ms_elapsed = (metrics['cycle_count'] / cpu_mhz) * 0.001 ;

        # ms_elapsed = metrics['time_report']

        # time = re.search('^real (\d+)[.](\d+)', stdout_str, re.S)
        # ms_elapsed = int(time.group(1)) * 1000 + \
        #              int('{:<03d}'.format(int(time.group(2)))) # 0-pad

        # Return value cannot be zero (will be interpreted as error)
        return max(float(ms_elapsed), 0.001)
    else:
        # We must have failed to find a time
        log.warning('Warning: Failed to find timing')
        return 0.0

def read_from_assembly(bench_name,search_term,check_term,bench_path):
    filename = bench_name+".disass"
    disassfile = path.join(bench_path,filename)
    instr_name = []
    if path.isfile(disassfile):
        with open(disassfile) as infile:
            for line in infile:
                if line.startswith(search_term+':'):
                    if(line.split("\t")[1].strip() == check_term):
                        instr_name = line.strip().split("\t")[2:]
                        instr_name[1] = instr_name[1]+'\t'
                    else :
                        instr_name = ['UKNWN','args unknownn']
                        instr_name[1] =   instr_name[1]+'\t'
        return instr_name

def read_from_disassembly(bench_name,bench_path):
    filename = bench_name+".disass"
    disassfile = path.join(bench_path,filename)
    if path.isfile(disassfile):
        bmi_instr = set(instrn_list)
        with open(disassfile) as infile:
            accumulator = []
            for line in infile:
                if line.strip().split("\t")[0].startswith('8') and not line.strip().split("\t")[0].endswith('>:'):
                    if line.strip().split("\t")[2] in bmi_instr :
                        # print(line.split())
                        accumulator.append(line.strip().split("\t")[2])
            cnt = Counter(accumulator)
            ins_list = cnt.most_common()
            ins_list.append(tuple(['=== Total ===',sum(cnt.values())]))
            # print('\nOccurences in disassembly \n'+displayresults(ins_list))
        return ins_list
