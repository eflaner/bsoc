/* Copyright (C) 2019 Clemson University

   Contributor Ola Jeppsson <ola.jeppsson@gmail.com>

   This file is part of Embench.

   SPDX-License-Identifier: GPL-3.0-or-later */

#include <support.h>
#include <encoding.h>
#include <stdint.h>

extern volatile int64_t prog_time,prog_cycl,prog_instr;
void
initialise_board ()
{
	prog_time = 0;
}

void __attribute__ ((noinline)) __attribute__ ((externally_visible))
start_trigger ()
{
	setStats(1);
  prog_time  -= ((uint64_t)read_csr(timeh) << 32) | (uint32_t)read_csr(time);
  	// prog_cycl  -= ((uint64_t)read_csr(mcycleh) << 32) | (uint32_t)read_csr(mcycle);
  	// prog_instr -= ((uint64_t)read_csr(minstreth) << 32) | (uint32_t)read_csr(minstret);
}

void __attribute__ ((noinline)) __attribute__ ((externally_visible))
stop_trigger ()
{
   // prog_instr  += ((uint64_t)read_csr(minstreth) << 32) | (uint32_t)read_csr(minstret);
   // prog_cycl   += ((uint64_t)read_csr(mcycleh) << 32) | (uint32_t)read_csr(mcycle);
  prog_time   += ((uint64_t)read_csr(timeh) << 32) | (uint32_t)read_csr(time);
	setStats(0);
}
