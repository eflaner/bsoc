/* Copyright (C) 2020 DoE CUSAT, Kerala

   Contributor Babu PS <babu.ps@hotmail.com>

   This file is part of bSoC adapted Shakti SDK.

   See LICENSE for more info */

#include "encoding.h"
#include <stdint.h>

#define NUM_COUNTERS 3
#define DISPLAY_OUTS NUM_COUNTERS+1
static int64_t counters[DISPLAY_OUTS];
static char* counter_names[DISPLAY_OUTS];

void
initialise_board ()
{

}

void setStats(int enable)
{
  int i = 0;
#define READ_CTR(nameh,name) do { \
    while (i >= NUM_COUNTERS) ; \
    int64_t csr = ((uint64_t)read_csr(nameh) << 32) | (uint32_t)read_csr(name);; \
    if (!enable) { csr -= counters[i]; counter_names[i] = #name; } \
    counters[i++] = csr; \
  } while (0)

  READ_CTR(timeh,time);
  READ_CTR(mcycleh,mcycle);
  READ_CTR(minstreth,minstret);

#undef READ_CTR
}

void __attribute__ ((noinline)) __attribute__ ((externally_visible))
start_trigger (){
	setStats(1);
}

void __attribute__ ((noinline)) __attribute__ ((externally_visible))
stop_trigger () {
	setStats(0);
}

void __attribute__ ((noinline)) __attribute__ ((externally_visible))
display_stats (int result){
  counter_names[NUM_COUNTERS] = "benchmark_result";
  counters[NUM_COUNTERS] = result;
  printf("%s = %d\n","benchmark_result",result);
  for (int i = 0; i < NUM_COUNTERS; i++)
    if (counters[i])
      printf("%s = %d\n",counter_names[i],counters[i]);
}
