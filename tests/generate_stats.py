#!/usr/bin/env python3

# Copyright (c) 2020, Babu P S All rights reserved.
# See LICENSE for more information

import sys
import fileinput
from tabulate import tabulate
from os import path,listdir
from collections import Counter
# All the common variables
com_var = dict()
instrn_list = ['andn', 'orn', 'xnor','slo','sro','rol','ror', 'sh1add','packu',\
'sh2add', 'sh3add', 'sbclr', 'sbset', 'sbinv', 'sbext', 'gorc', 'grev', 'sloi',\
'rori', 'sbclri', 'sbseti', 'sbexti', 'sbinvi', 'gorci','grevi', 'cmix','cmov',\
'sroi','fsl', 'fsr', 'fsri', 'clz','ctz', 'pcnt', 'sext.b','sext.h', 'crc32.b',\
'crc32.h','crc32.w','crc32c.b','crc32c.h', 'crc32c.w', 'clmul', 'clmulr','min',\
'clmulh',  'max', 'minu', 'maxu', 'shfl','unshfl','bdep', 'bext', 'pack','bfp',\
'packh', 'shfli', 'unshfli']

def main(arguments):
	com_var['rootdir'] = path.abspath(path.dirname(__file__))
	com_var['benchmarks_dir'] = path.join(com_var['rootdir'], 'workdir')
	collect_benchmarks(com_var['benchmarks_dir'])

def exitpoint():
	"""Unified Point of Exit.
	"""
	print("Operation Aborted. Exiting .. .. ..")
	sys.exit(1)

def collect_benchmarks(benchmark_path):
	dirlist = listdir(benchmark_path)
	keyword = "Accounted"
	for bench_name in dirlist:
		bench_dir = path.join(benchmark_path, bench_name)
		for f in listdir(bench_dir):
			if f.endswith(bench_name+".log"):
## Modify the log with the pseudoynm of the instruction
				accumulator = []
				for line in fileinput.input(path.join(bench_dir,f), inplace=True):
					if line.lstrip().startswith("SCOP"):
						search_term = line.split("\t")[1]
						check_term = line.split("\t")[2]
						pseudoynm = read_from_assembly(bench_name,search_term,check_term,bench_dir)
						accumulator.append(pseudoynm[0])
						line = line.replace(line,'\t'.join(line.split("\t")[1:3]+pseudoynm+line.split("\t")[3:]))
					print(line, end='')
## No modification of Log file - yet display the frequency of instructions.
				# with open(path.join(bench_dir,f)) as infile:
				# 	accumulator = []
				# 	for line in infile:
				# 		if line.lstrip().startswith("SCOP"):
				# 			search_term = line.split("\t")[1]
				# 			check_term = line.split("\t")[2]
				# 			pseudoynm = read_from_assembly(bench_name,search_term.strip(),check_term.strip(),bench_dir)
				# 			# print("{:<10} | {:<30} | {:<10} | {:<20}".format(search_term,' '.join(pseudoynm),check_term,bench_name))
				# 			accumulator.append(pseudoynm[0])
				# 			# print(line.split("\t")[1:3]+pseudoynm+line.split("\t")[3:])
				cnt = Counter(accumulator)
				ins_list = cnt.most_common()
				ins_list.append(tuple(['=== Total ===',sum(cnt.values())]))
				ins_dis_list = read_from_disassembly(bench_name,bench_dir)
				print('\nOccurences in Trace       \n'+displayresults(ins_list))
				with open(path.join(bench_dir,f), "a") as myfile:
					myfile.write('\nOccurences in Trace       \n'+displayresults(ins_list))
					myfile.write('\nOccurences in disassembly \n'+displayresults(ins_dis_list))

def read_from_assembly(bench_name,search_term,check_term,bench_path):
	filename = bench_name+".disass"
	disassfile = path.join(bench_path,filename)
	instr_name = []
	if path.isfile(disassfile):
		with open(disassfile) as infile:
			for line in infile:
				if line.startswith(search_term+':'):
					if(line.split("\t")[1].strip() == check_term):
						instr_name = line.strip().split("\t")[2:]
						instr_name[1] = instr_name[1]+'\t'
		return instr_name
	else:
		print(" not found Check whether directory Exists")
		exitpoint()

def read_from_disassembly(bench_name,bench_path):
	filename = bench_name+".disass"
	disassfile = path.join(bench_path,filename)
	if path.isfile(disassfile):
		bmi_instr = set(instrn_list)
		with open(disassfile) as infile:
			accumulator = []
			print(bench_name)
			for line in infile:
				if line.strip().split("\t")[0].startswith('8') and not line.strip().split("\t")[0].endswith('>:'):
					if line.strip().split("\t")[2] in bmi_instr :
						# print(line.split())
						accumulator.append(line.strip().split("\t")[2])
			cnt = Counter(accumulator)
			ins_list = cnt.most_common()
			ins_list.append(tuple(['=== Total ===',sum(cnt.values())]))
			print('\nOccurences in disassembly \n'+displayresults(ins_list))
		return ins_list
	else:
		print(" not found Check whether directory Exists")
		exitpoint()

def displayresults(table):
	headers = ['Instruction', 'count']
	return tabulate(table, headers, tablefmt="psql")

if __name__ == "__main__":
	sys.exit(main(sys.argv[1:]))
