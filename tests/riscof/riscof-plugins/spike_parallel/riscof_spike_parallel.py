import os
import re
import shutil
import subprocess
import shlex
import logging
import random
import string
from string import Template
import sys

import riscof.utils as utils
from riscof.pluginTemplate import pluginTemplate
import riscof.constants as constants

logger = logging.getLogger()

class spike_parallel(pluginTemplate):
    __model__ = "Spike"
    __version__ = "0.5.0"

    def __init__(self, *args, **kwargs):
        sclass = super().__init__(*args, **kwargs)

        config = kwargs.get('config')
        if config is None:
            print("Please enter input file paths in configuration.")
            raise SystemExit
        else:
            self.isa_spec = os.path.abspath(config['ispec'])
            self.platform_spec = os.path.abspath(config['pspec'])
            self.pluginpath = os.path.abspath(config['pluginpath'])

        return sclass

    def initialise(self, suite, work_dir, compliance_env):
        if shutil.which('spike') is None:
            logger.error('Please install spike to proceed further')
            sys.exit(0)
        self.work_dir = work_dir
        self.compile_cmd = 'riscv64-unknown-elf-gcc -march={0} -mabi=ilp32 \
         -static -mcmodel=medany -fvisibility=hidden -nostdlib -nostartfiles\
         -T '+self.pluginpath+'/env/link.ld\
         -I '+self.pluginpath+'/env/\
         -I ' + compliance_env

    def build(self, isa_yaml, platform_yaml):
        ispec = utils.load_yaml(isa_yaml)
        self.isa = ispec["ISA"]

    def runTests(self, testList):
        make = utils.makeUtil(makefilePath=os.path.join(self.work_dir, "Makefile." + self.name[:-1]))
        #make.makeCommand = 'make -j8'
        #make.makeCommand = 'pmake -j 8'
        for file in testList:
            testentry = testList[file]
            test = testentry['test_path']
            test_dir = testentry['work_dir']

            elf = 'my.elf'

            execute = "cd "+testentry['work_dir']+";"

            cmd = self.compile_cmd.format(testentry['isa'].lower()) + ' ' + test + ' -o ' + elf
            compile_cmd = cmd + ' -D' + " -D".join(testentry['macros'])
            execute+=compile_cmd+";"

            execute += 'spike --isa={0} +signature=sign {1};'.format(self.isa, elf)

            sign_fix = 'sh '+self.pluginpath+'/env/sign_fix.sh'
            execute+=sign_fix+";"

            rename_sign = 'cat sign > ' + os.path.join(test_dir, self.name[:-1] + ".signature")
            execute+=rename_sign+";"
            make.add_target(execute)
        make.execute_all(self.work_dir)
