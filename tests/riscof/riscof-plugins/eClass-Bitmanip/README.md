### Building bSoC simulation executable

Make sure you have verilator (v4.000 and above), dtc-1.4.7 and BLUESPEC installed.
```
git clone git@gitlab.com:shaktiproject/cores/e-class.git
cd e-class/base-sim
./manager.sh update_deps
make 
```

This above will generate a `out` binary in the bin folder along with `boot.*` files.


### Riscof Plugin for bSoC

- Config file entry
```
DUTPlugin=eClass

[eClass]
eClassBin=/path_to_bSoC_bin
```

- Export command
```
pwd=pwd;export PYTHONPATH=$pwd:$PYTHONPATH;
```