#include "compliance_test.h"
#include "compliance_model.h"

RVTEST_ISA("RV32IMB")

RVTEST_CODE_BEGIN

    RVMODEL_IO_INIT
    RVMODEL_IO_ASSERT_GPR_EQ(x31, x0, 0x00000000)
    RVMODEL_IO_WRITE_STR(x31, "# Test Begin\n")

    # ---------------------------------------------------------------------------------------------
  #ifdef TEST_CASE_1
    RVTEST_CASE(1,"// check ISA:=regex(.*B.*); def TEST_CASE_1=True")
    la a1,test_results
    li s4, 0x522a1063
    li s5, 0xd2d6b642
    shfl s11,s4,s5
    sw s11,0(a1)
    li s2, 0x8a671b8c
    li a0, 0x5b4289cf
    shfl s7,s2,a0
    sw s7,4(a1)
    li t2, 0x02eb0801
    li s6, 0x81920806
    shfl s0,t2,s6
    sw s0,8(a1)
    li t0, 0x43737b61
    li s7, 0x57861f11
    shfl s1,t0,s7
    sw s1,12(a1)
    li s1, 0xf20d8a82
    li a0, 0x1ceb0c11
    shfl t2,s1,a0
    sw t2,16(a1)
    li a6, 0x9543c51a
    li t5, 0x9a48a02c
    shfl s10,a6,t5
    sw s10,20(a1)
    li s7, 0x00d321db
    li s0, 0x4ae7185c
    shfl t1,s7,s0
    sw t1,24(a1)
    li s1, 0x0f3ff336
    li s9, 0x4b10a48e
    shfl a6,s1,s9
    sw a6,28(a1)
    li a2, 0xf807f882
    li t3, 0xa200000e
    shfl a3,a2,t3
    sw a3,32(a1)
    sw zero,36(a1)
    sw zero,40(a1)
    sw zero,44(a1)
    RVMODEL_IO_WRITE_STR(x31, "# Test End\n")

  #endif
 # ---------------------------------------------------------------------------------------------
    # HALT
    RVMODEL_HALT
RVTEST_CODE_END

.data
RVMODEL_DATA_BEGIN
    .align 4
test_results:
.fill 12,4,-1
RVMODEL_DATA_END
