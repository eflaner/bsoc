#include "compliance_test.h"
#include "compliance_model.h"

RVTEST_ISA("RV32IMB")

RVTEST_CODE_BEGIN

    RVMODEL_IO_INIT
    RVMODEL_IO_ASSERT_GPR_EQ(x31, x0, 0x00000000)
    RVMODEL_IO_WRITE_STR(x31, "# Test Begin\n")

    # ---------------------------------------------------------------------------------------------
  #ifdef TEST_CASE_1
    RVTEST_CASE(1,"// check ISA:=regex(.*B.*); def TEST_CASE_1=True")
    la a1,test_results
    li s8, 0x522a1063
    sroi a6,s8,3
    sw a6,0(a1)
    li t1, 0x8a671b8c
    sroi s3,t1,5
    sw s3,4(a1)
    li s9, 0x02eb0801
    sroi s3,s9,17
    sw s3,8(a1)
    li s10, 0x43737b61
    sroi s11,s10,24
    sw s11,12(a1)
    li t2, 0xf20d8a82
    sroi s6,t2,25
    sw s6,16(a1)
    li t5, 0x9543c51a
    sroi s11,t5,31
    sw s11,20(a1)
    li s11, 0xc8680132
    sroi s7,s11,29
    sw s7,24(a1)
    sw zero,28(a1)
    RVMODEL_IO_WRITE_STR(x31, "# Test End\n")

  #endif
 # ---------------------------------------------------------------------------------------------
    # HALT
    RVMODEL_HALT
RVTEST_CODE_END

.data
RVMODEL_DATA_BEGIN
    .align 4
test_results:
.fill 8,4,-1
RVMODEL_DATA_END
